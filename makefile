export CONAN_VERBOSE_TRACEBACK=1
export CONAN_PRINT_RUN_COMMANDS=1

cpkgs=$(notdir $(wildcard pkg/*))
$(info cpkgs=$(cpkgs))
$(info cpkgs=$(cpkgs:%=cpkg-clean-%))

## cpkg

.PHONY: cpkg-create cpkg-upload cpkg-clean
cpkg-create: $(cpkgs:%=cpkg-create-%)
cpkg-upload: $(cpkgs:%=cpkg-upload-%)
cpkg-uploadall: $(cpkgs:%=cpkg-uploadall-%)
cpkg-clean: $(cpkgs:%=cpkg-clean-%)

conan_remote=localhost
conan_remote_arg=--remote $(conan_remote)

CONAN_ARGS=
CONAN=conan
CONAN_CMD_ARGS=

.PHONY: cpkg-create-%
cpkg-create-%:
	CONAN_PRINT_RUN_COMMANDS=1 $(CONAN) $(CONAN_ARGS) create $(CONAN_CMD_ARGS) --keep-source pkg/$*/ xadix/default

.PHONY: cpkg-upload-%
cpkg-upload-%: cpkg-create-%
	$(CONAN) upload $(CONAN_CMD_ARGS) --confirm $(conan_remote_arg) '$*/*@xadix/*'

.PHONY: cpkg-uploadall-%
cpkg-uploadall-%: cpkg-create-%
	$(CONAN) upload $(CONAN_CMD_ARGS) --confirm --all $(conan_remote_arg) '$*/*@xadix/*'

# https://docs.conan.io/en/latest/reference/commands/creator/create.html

.PHONY: cpkg-clean-%
cpkg-clean-%:
	! test -e pkg/$*/build-conan || rm -vr pkg/$*/build-conan

.PHONY: cpkg-install-%
cpkg-install-%: cpkg-clean-%
	CONAN_PRINT_RUN_COMMANDS=1 $(CONAN) install $(CONAN_CMD_ARGS) --build=outdated --install-folder=pkg/$*/build-conan pkg/$*/

.PHONY: cpkg-source-%
cpkg-source-%: cpkg-install-%
	CONAN_PRINT_RUN_COMMANDS=1 $(CONAN) source $(CONAN_CMD_ARGS) --install-folder=pkg/$*/build-conan --source-folder=pkg/$*/build-conan pkg/$*/

.PHONY: cpkg-build-%
cpkg-build-%: cpkg-source-%
	CONAN_PRINT_RUN_COMMANDS=1 $(CONAN) build $(CONAN_CMD_ARGS) --install-folder=pkg/$*/build-conan --source-folder=pkg/$*/build-conan --build-folder=pkg/$*/build-conan pkg/$*/

.PHONY: cpkg-package-%
cpkg-package-%: cpkg-build-%
	CONAN_PRINT_RUN_COMMANDS=1 $(CONAN) package $(CONAN_CMD_ARGS) --install-folder=pkg/$*/build-conan --source-folder=pkg/$*/build-conan --build-folder=pkg/$*/build-conan pkg/$*/

## cdata

.PHONY: cdata-clean cdata-ls
cdata-clean: $(cpkgs:%=cdata-clean-%)
cdata-ls: $(cpkgs:%=cdata-ls-%)

.PHONY: cdata-clean-%
cdata-clean-%:
	! test -e ~/.conan/data/$* || rm -fvr ~/.conan/data/$*

.PHONY: cdata-ls-%
cdata-ls-%:
	! test -e ~/.conan/data/$*/*/*/*/package/* || ls -ld ~/.conan/data/$*/*/*/*/package/*

## cx

.PHONY: cx-upload
cx-upload:
	$(CONAN) search -j /dev/stderr 2>&1 1>/dev/null | jq -r '.results[].items[].recipe.id' | tr '\n' '\000' | xargs -0 -n1 conan upload --confirm --all $(conan_remote_arg)

################################################################################
## clean
################################################################################

clean:
	rm -r conan-use/*/build-conan/; true
