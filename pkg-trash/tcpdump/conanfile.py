#!/usr/bin/env python3

# https://docs.conan.io/en/latest/

import os
import os.path
import stat
import re
import sys
import logging
import conans
import json
import conans.errors
import conans.model.version
import conans.errors
import urllib.request
import inspect
import traceback

logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%dT%H:%M:%S', stream=sys.stderr, format="%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s %(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s")

# https://bintray.com/groovy/maven/groovy

available_versions = [
    "4.9.2",
    "4.8.1",
    "3.9.8",
]

# https://docs.conan.io/en/latest/reference/conanfile.html
# https://docs.conan.io/en/latest/reference/conanfile/attributes.html
# https://docs.conan.io/en/latest/reference/conanfile/methods.html
# https://docs.conan.io/en/latest/reference/conanfile/other.html

# https://docs.conan.io/en/latest/reference/commands/creator/create.html
# https://docs.conan.io/en/latest/reference/commands/creator/upload.html
class TheConan(conans.ConanFile):
    name = "tcpdump"
    version = [v for v in available_versions if "-" not in v][0]
    description = "..."
    homepage = "..."
    url = "..."
    license = "..."
    author = "...."
    topics = ()
    default_user = "xadix"
    default_channel = "default"
    settings = { "os": [ "Linux" ], "arch": [ "x86_64" ] }
    build_policy = "missing"
    no_copy_source = True
    options = {
        "version": available_versions,
        "rpath": [True, False],
        "shared": [True, False],
        "rshared": [True, False],
    }
    default_options = {
        "version": [v for v in available_versions if "-" not in v][0],
        "rpath": False,
        "shared": False,
        "rshared": False,
    }

    @property
    def arch(self):
        return self.settings.get_safe("arch_build") or self.settings.get_safe("arch")

    @property
    def os(self):
        return self.settings.get_safe("os_build") or self.settings.get_safe("os")

    def config_options(self):
        logging.info("config_options: ...")

    def configure(self):
        logging.info("configure: ...")
        if self.os != "Linux" and self.arch != "x86_64":
            raise Exception("Unsupported platform and OS")
        

    # https://docs.conan.io/en/latest/reference/conanfile/attributes.html#requires
    # https://docs.conan.io/en/latest/mastering/version_ranges.html#version-ranges
    def requirements(self):
        logging.info("requirements: ...")
        self.requires("libpcap/[>=1.8]@bincrafters/stable")
        self.requires("OpenSSL/[>=1.1]@conan/stable")

        if ( options )

    def build_requirements(self):
        logging.info("build_requirements: ...")

    def system_requirements(self):
        logging.info("system_requirements: ...")

    @property
    def source_dirname(self):
        return "tcpdump-{}".format( self.version )

    @property
    def source_filename(self):
        return "tcpdump-{}.tar.gz".format( self.version )

    @property
    def source_url(self):
        return "https://www.tcpdump.org/release/tcpdump-{}.tar.gz".format( self.version )

    def source(self):
        logging.info("source: ...")

        if not os.path.isfile(self.source_filename):
            conans.tools.download(self.source_url, self.source_filename, overwrite=False)

    def imports(self):
        logging.info("imports: ...")

    def build(self):
        logging.info("build: ...")
        conans.tools.unzip(os.path.join(self.source_folder, self.source_filename))
        #pcap_config_file = 'pcap-config.exe' if self.settings.os == 'Windows' else 'pcap-config'
        #pcap_config_path = os.path.join(self.deps_cpp_info["libpcap"].bin_paths[0], pcap_config_file)

        #use_ldflags=[]

        #for dep in self.deps_cpp_info.deps:
        #    use_ldflags.extend(['-L'+path for path in self.deps_cpp_info[dep].lib_paths])

        with conans.tools.chdir(self.source_dirname):

            env_build = conans.AutoToolsBuildEnvironment(self, win_bash=False, include_rpath_flags=self.options.rpath)

            args = []
            args.append("--with-system-libpcap")
            #if self.options.shared:
            #    args.extend(['--enable-shared=yes', '--enable-static=no'])
            #else:
            #    args.extend(['--enable-shared=no', '--enable-static=yes'])

            env_build.configure(args=args)
            env_build.make()
            env_build.install()

    def package(self):
        logging.info("package: %s", os.getcwd())
        self.copy(pattern="*", dst=".", src="pdir")

    def package_info(self):
        logging.info("package_info: ...")
        #logging.info("package_info: %s", "\n".join( [ str(frame) for frame in inspect.stack() ] ) )
        if self.package_folder is not None:
            self.env_info.path.append(os.path.join(self.package_folder, "bin"))
            #self.env_info.MANPATH.append(os.path.join(self.package_folder, "share", "man"))


    def deploy(self):
        logging.info("deploy: ...")
