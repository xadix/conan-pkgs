# ...

https://bintray.com/beta/#/xadix/xadix-conan?tab=packages

```bash
## upload generators ...
make cpkg-uploadall-gnumake_generator
make cpkg-uploadall-debug_generator

## upload packages ...
make cpkg-uploadall-openjre_prebuilt
make cpkg-uploadall-groovy_prebuilt

make conan_remote=xadix-conan cpkg-uploadall-{gnumake_generator,debug_generator,openjre_prebuilt,groovy_prebuilt}
make conan_remote=xadix-conan cpkg-upload-{gnumake_generator,debug_generator,openjre_prebuilt,groovy_prebuilt}

make cpkg-upload
make cpkg-upload conan_remote=xadix-conan
```

```bash
vim ~/.conan_server/server.confa
conan_server
conan remote remove localhost
conan remote add localhost http://localhost:9300/
conan user --remote localhost --password demo demo
conan remote list
```
