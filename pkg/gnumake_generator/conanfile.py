# vim: set ft=python sts=4 ts=4 sw=4 expandtab fo-=t:
import os
import platform

from conans import ConanFile
from conans.model import Generator
import conans

class GnuMakeGeneratorBuilder(object):
    def __init__( self ):
        self.parts = []

    def _prefix_to_string(self, prefix):
        if prefix is None or len(prefix) == 0:
            return ""
        return prefix + "_"

    def append_variable( self, prefix, key, value ):
        if isinstance( value, list ):
            self.parts.append( "%s%s=\\\n\t" % ( self._prefix_to_string( prefix ), key ) )
            self.parts.append( " \\\n\t".join( value ) )
            self.parts.append( "\n\n" )
        else:
            self.parts.append( "%s%s=%s\n\n" % ( self._prefix_to_string( prefix ), key, value ) )

    def append_class( self, prefix, value ):
        if isinstance( value, conans.model.build_info._CppInfo ):
            self.append_variable( prefix, "includedirs", value.includedirs )
            self.append_variable( prefix, "libdirs", value.libdirs )
            self.append_variable( prefix, "resdirs", value.resdirs )
            self.append_variable( prefix, "bindirs", value.bindirs )
            self.append_variable( prefix, "builddirs", value.builddirs )

            self.append_variable( prefix, "libs", value.libs )
            self.append_variable( prefix, "defines", value.defines )
            self.append_variable( prefix, "cflags", value.cflags )
            self.append_variable( prefix, "cppflags", value.cppflags )
            self.append_variable( prefix, "sharedlinkflags", value.sharedlinkflags )
            self.append_variable( prefix, "exelinkflags", value.exelinkflags )
            self.append_variable( prefix, "rootpath", value.rootpath )
            self.append_variable( prefix, "sysroot", value.sysroot )

            self.append_variable( prefix, "include_paths", value.include_paths )
            self.append_variable( prefix, "lib_paths", value.lib_paths )
            self.append_variable( prefix, "bin_paths", value.bin_paths )
            self.append_variable( prefix, "build_paths", value.build_paths )
            self.append_variable( prefix, "res_paths", value.res_paths )
        if isinstance( value, conans.model.build_info.CppInfo ):
            self.append_variable( prefix, "public_deps", value.public_deps )
            self.append_variable( prefix, "configs", value.configs )
        if isinstance( value, conans.model.build_info._BaseDepsCppInfo ):
            None
        if isinstance( value, conans.model.build_info.DepsCppInfo ):
            self.append_variable( prefix, "configs", value.configs )
            self.append_variable( prefix, "deps", value.deps )
            for dkey, dvalue in value.dependencies:
                self.append_class( "%s_%s" % ( prefix, dkey ), dvalue )


class GnuMakeGenerator(Generator):

    def __init__(self, conanfile):
        super(GnuMakeGenerator, self).__init__(conanfile)
        self.conanfile = conanfile
        self.env = conanfile.env

    @property
    def filename(self):
        return "conan_gnumake.mk"

    @classmethod
    def _append_list(parts, lst):
        parts.append(" \\\n\t".join(lst))

    @classmethod
    def _append_variable(parts, key, value):
        if isinstance( value, list ):
            parts.append("%s=\\\n\t" % key)
            _append_list(parts, value)
            parts.append("\n") 
        else:
            parts.append("%s=%s\n" % ( key, value ))

    @classmethod
    def _append_class( parts, prefix, value ):
        #if isinstance( value, conans.model.build_info.DepsEnvInfo ):
        #if isinstance( value, conans.model.build_info.EnvInfo ):
        #if isinstance( value, conans.model.build_info.UserInfo ):
        None

    @property
    def content(self):
        #json.dumps({"c": 0, "b": 0, "a": 0}, sort_keys=True)
        #lines.append("conan_include_paths=\\\n\t%s" % ( " \\\n\t".join(self.deps_build_info.include_paths) ))
        builder = GnuMakeGeneratorBuilder()
        #builder.append_class("conan_build_info", self.build_info)
        #builder.append_class("conan_deps_build_info", self.deps_build_info)
        return "".join(builder.parts)

class TheConanFile(ConanFile):
    name = "gnumake_generator"
    version = "0.2.0"
    default_user = "xadix"
    default_channel = "default"
    build_policy = "missing"

    def build(self):
      pass

    def package_info(self):
      self.cpp_info.includedirs = []
      self.cpp_info.libdirs = []
      self.cpp_info.bindirs = []
