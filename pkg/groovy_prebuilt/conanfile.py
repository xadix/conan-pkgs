#!/usr/bin/env python3

# https://docs.conan.io/en/latest/

import os
import os.path
import stat
import re
import sys
import logging
import conans
import json
import conans.errors
import conans.model.version
import conans.errors
import urllib.request
import inspect
import traceback

logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%dT%H:%M:%S', stream=sys.stderr, format="%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s %(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s")

# https://bintray.com/groovy/maven/groovy

available_versions = [
    "2.5.6",
    "2.4.16",
]

# https://docs.conan.io/en/latest/reference/conanfile.html
# https://docs.conan.io/en/latest/reference/conanfile/attributes.html
# https://docs.conan.io/en/latest/reference/conanfile/methods.html
# https://docs.conan.io/en/latest/reference/conanfile/other.html

# https://docs.conan.io/en/latest/reference/commands/creator/create.html
# https://docs.conan.io/en/latest/reference/commands/creator/upload.html
class TheConan(conans.ConanFile):
    name = "groovy_prebuilt"
    version = [v for v in available_versions if "-" not in v][0]
    description = "..."
    homepage = "..."
    url = "..."
    license = "..."
    author = "...."
    topics = ()
    default_user = "xadix"
    default_channel = "default"
    settings = { "os": [ "Linux" ], "arch": [ "x86_64" ] }
    build_policy = "missing"
    no_copy_source = True
    options = {"version": available_versions}
    default_options = { "version": [v for v in available_versions if "-" not in v][0] }

    @property
    def arch(self):
        return self.settings.get_safe("arch_build") or self.settings.get_safe("arch")

    @property
    def os(self):
        return self.settings.get_safe("os_build") or self.settings.get_safe("os")


    def configure(self):
        logging.info("configure: ...")
        if self.os != "Linux" and self.arch != "x86_64":
            raise Exception("Unsupported platform and OS")

        # https://dl.bintray.com/groovy/maven/apache-groovy-binary-2.5.5.zip
        # https://dl.bintray.com/groovy/maven/apache-groovy-binary-2.5.6.zip
        # https://dl.bintray.com/groovy/maven/apache-groovy-binary-3.0.0-alpha-4.zip
        binary_dir = "groovy-{:s}".format(self.version)
        binary_name = "apache-groovy-binary-{:s}.zip".format(self.version)
        binary_link = "https://dl.bintray.com/groovy/maven/{:s}".format(binary_name)
        self._binary = { "binary_dir": binary_dir, "binary_name": binary_name, "binary_link": binary_link }
        logging.info("binary = %s", self._binary)

    def source(self):
        logging.info("source: ...")

        if not os.path.isfile(self._binary["binary_name"]):
            conans.tools.download(self._binary["binary_link"], self._binary["binary_name"], overwrite=False)


    def build(self):
        logging.info("build: ...")
        conans.tools.unzip(os.path.join(self.source_folder, self._binary["binary_name"]))
        os.rename(self._binary["binary_dir"], "pdir")
        exec_files = [
            "grape", "groovy", "groovyc",
            "groovyConsole", "groovydoc", "groovysh",
            "java2groovy", "startGroovy",
        ]
        for exec_file in exec_files:
            fpath = "pdir/bin/{:s}".format( exec_file )
            fstat = os.stat(fpath)
            os.chmod(fpath, fstat.st_mode | stat.S_IEXEC)
        #os.chmod("pdir/bin/grape", stat.S_IEXEC)
        #os.chmod("pdir/bin/groovy", stat.S_IEXEC)
        #os.chmod("pdir/bin/groovyc", stat.S_IEXEC)

    def package(self):
        logging.info("package: %s", os.getcwd())
        self.copy(pattern="*", dst=".", src="pdir")

    def package_info(self):
        logging.info("package_info: ...")
        #logging.info("package_info: %s", "\n".join( [ str(frame) for frame in inspect.stack() ] ) )
        if self.package_folder is not None:
            self.env_info.path.append(os.path.join(self.package_folder, "bin"))
            #self.env_info.MANPATH.append(os.path.join(self.package_folder, "share", "man"))

    # https://docs.conan.io/en/latest/reference/conanfile/attributes.html#requires
    # https://docs.conan.io/en/latest/mastering/version_ranges.html#version-ranges
    def requirements(self):
        logging.info("requirements: ...")
        self.requires("openjre_prebuilt/[>=8]@xadix/default")

    def build_requirements(self):
        logging.info("build_requirements: ...")

    def system_requirements(self):
        logging.info("system_requirements: ...")

    def imports(self):
        logging.info("imports: ...")

    def deploy(self):
        logging.info("deploy: ...")
