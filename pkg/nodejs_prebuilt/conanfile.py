#!/usr/bin/env python3

# https://docs.conan.io/en/latest/

import os
import os.path
import re
import sys
import logging
import conans
import json
import conans.errors
import conans.model.version
import conans.errors
import urllib.request

logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%dT%H:%M:%S', stream=sys.stderr, format="%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s %(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s")


available_versions = [
    "10.15.1",
    "11.9.0",
    "9.11.2",
    "8.15.0",
]
# https://docs.conan.io/en/latest/reference/conanfile.html
# https://docs.conan.io/en/latest/reference/conanfile/attributes.html
# https://docs.conan.io/en/latest/reference/conanfile/methods.html
# https://docs.conan.io/en/latest/reference/conanfile/other.html

# https://docs.conan.io/en/latest/reference/commands/creator/create.html
# https://docs.conan.io/en/latest/reference/commands/creator/upload.html
class TheConan(conans.ConanFile):
    name = "nodejs_prebuilt"
    version = [v for v in available_versions if "-" not in v][0]
    description = "..."
    homepage = "..."
    url = "..."
    license = "..."
    author = "...."
    topics = ()
    default_user = "xadix"
    default_channel = "default"
    settings = { "os": [ "Linux" ], "arch": [ "x86_64" ] }
    build_policy = "missing"
    no_copy_source = True
    options = {"version": available_versions}
    default_options = { "version": [v for v in available_versions if "-" not in v][0] }

    @property
    def arch(self):
        return self.settings.get_safe("arch_build") or self.settings.get_safe("arch")

    @property
    def os(self):
        return self.settings.get_safe("os_build") or self.settings.get_safe("os")


    def configure(self):
        logging.info("configure: ...")
        if self.os != "Linux" and self.arch != "x86_64":
            raise Exception("Unsupported platform and OS")

        binary_dir = "node-v{:s}-linux-x64".format(self.version)
        binary_name = "node-v{:s}-linux-x64.tar.xz".format(self.version)
        binary_link = "https://nodejs.org/dist/v{:s}/{:s}".format(self.version, binary_name)
        self._binary = { "binary_dir": binary_dir, "binary_name": binary_name, "binary_link": binary_link }
        logging.info("binary = %s", self._binary)

    def source(self):
        logging.info("source: ...")

        if not os.path.isfile(self._binary["binary_name"]):
            conans.tools.download(self._binary["binary_link"], self._binary["binary_name"], overwrite=False)


    def build(self):
        logging.info("build: ...")
        conans.tools.unzip(os.path.join(self.source_folder, self._binary["binary_name"]))
        os.rename(self._binary["binary_dir"], "pdir")

    def package(self):
        logging.info("package: ...")
        self.copy(pattern="*", dst=".", src="pdir")

    def package_info(self):
        logging.info("entry")
        if self.package_folder is not None:
            self.env_info.path.append(os.path.join(self.package_folder, "bin"))
            self.env_info.MANPATH.append(os.path.join(self.package_folder, "share", "man"))

    def requirements(self):
        logging.info("requirements: ...")

    def build_requirements(self):
        logging.info("build_requirements: ...")

    def system_requirements(self):
        logging.info("system_requirements: ...")

    def imports(self):
        logging.info("imports: ...")

    def deploy(self):
        logging.info("deploy: ...")
